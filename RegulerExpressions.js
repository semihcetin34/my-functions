const isEmailCorrect=function(value){
	const email=/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return email.test(value);
}
const hasLowerCase=function(value){
	const loverCase=/[a-z]/g;
	return loverCase.test(value);
}
//hasUpperCase Reguler Expression
const hasUpperCase=function(value){
	const upperCase=/[A-Z]/g;
	return upperCase.test(value);
}
//hasDigitCase Reguler Expression
const hasNumberCase=function(value){
	const numberCase=/[0-9]/g;
	return numberCase.test(value);
}
const isFloat=function(value){
	const floatCase=/^\d*\.?\d+(\d)?$/
	return floatCase.test(value);
}
const isNumber=function(value){
	let a=new RegExp("[0-9][0-9]*");//dinamik reguler expression
	let sonuc=value.match(a);
	if(sonuc===null) return false;
	else return value==sonuc[0];
}

export default {
	
	hasLowerCase,
	hasUpperCase,
	hasNumberCase,
	isEmailCorrect,
	isFloat,
	isNumber
	
}