// Bu kütüphaneyi kullanır iken localstorageden tokeni aramaktadır.
/*let Message={
	Success:    function(Message){
		//alert(Message);
		toastr.success(Message);
	},
	Error:      function(Message){
		//alert(Message);
		toastr.error('Error',Message);
	},
	Warning:    function(Message){
		//alert(Message);
		toastr.warning(Message)
	},
	Info:       function(Message){
		//alert(Message);
		toastr.info(Message);
	}
}*/
let xhrStatus=function(xhr){
	if(xhr.status==200){
		let response=JSON.parse(xhr.response);
	/*	if(response.Result.IsSuccess!=undefined && response.Result.IsSuccess==true) Message.Success(response.Result.ResponseMessage);
		else if(response.Result.IsSuccess!=undefined && response.Result.IsSuccess==false) Message.Warning(response.Result.ResponseMessage);
*/
		return "success";
	}
	else
		return "fail";
}

const FintechPost={
	FormData:function(action,version,data){
		let paramettesrs={
			action:action,
			version:version,
			parameters:{
				...data
			}
		}
		let define={
			request:JSON.stringify(paramettesrs)
		}
		return PostForm(define,'FinTech/ExecuteFormData')
	}
}
const PostForm=function(object,url){
	let xhr = new XMLHttpRequest();
	xhr.open('POST', _ApiUrl+url,true);
	xhr.setRequestHeader("Authorization",'Bearer '+localStorage.getItem('token'));
	let keys=Object.keys(object);
	let fd = new FormData();
	for (let i = 0; i <keys.length ; i++) {
		fd.append(keys[i],object[keys[i]]);
	}
	return new Promise(function (resolve,reject) {
		xhr.send(fd);
		xhr.onload = function() {
			if (xhr.status != 200) { // analyze HTTP status of the response
				if(JSON.parse(xhr.response).ResultType!==undefined)
					resolve(xhr.response);
				else
					reject(xhr.statusText);
				} else { // show the result
				resolve(xhr.response);
				}
		};
		xhr.onprogress = function(event) {
			//if (event.lengthComputable) {console.log(`Received ${event.loaded} of ${event.total} bytes`);
			//} else { console.log(`Received ${event.loaded} bytes`); // no Content-Length}
		};
		xhr.onerror = function(er) {
			reject(er)
		};
	});
}
const PostFormSystem = function(object,url){
	let xhr = new XMLHttpRequest();
	xhr.open('POST', _ApiUrl+url,true);
	xhr.setRequestHeader("Authorization",'Bearer '+localStorage.getItem('token'));
	let keys=Object.keys(object);
	let fd = new FormData();
	for (let i = 0; i <keys.length ; i++) {
		fd.append(keys[i],object[keys[i]]);
	}
	return new Promise(function (resolve,reject) {
		xhr.send(fd);
		xhr.onload = function() {
			let result=xhrStatus(xhr);
			if (result =="success") {
				resolve(xhr.response);
				//reject(xhr.statusText);
			} else { // show the result
				resolve(xhr.response);
				console.log(`Done, got ${xhr.response.length} bytes`); // responseText is the server
			}
		};
		xhr.onprogress = function(event) {
			if (event.lengthComputable) {
			} else {
			}
		};
		xhr.onerror = function(er) {
			reject(er)
		};
	});
}
const PostUrl = function(url){
	let xhr = new XMLHttpRequest();
	xhr.open('POST', _ApiUrl+url,true);
	xhr.setRequestHeader("Authorization",'Bearer '+localStorage.getItem('token'));
	return new Promise(function (resolve,reject) {
		xhr.send();
		xhr.onload = function() {
			if (xhr.status != 200) { // analyze HTTP status of the response
				//console.log(`Error ${xhr.status}: ${xhr.statusText}`); // e.g. 404: Not Found
				reject(xhr.statusText);
			} else { // show the result
				resolve(xhr.response);
				//console.log(`Done, got ${xhr.response.length} bytes`); // responseText is the server
			}
		};
		xhr.onprogress = function(event) {
			if (event.lengthComputable) {
				//console.log(`Received ${event.loaded} of ${event.total} bytes`);
			} else {
				//console.log(`Received ${event.loaded} bytes`); // no Content-Length
			}
		};
		xhr.onerror = function(er) {
			reject(er)
		};
	});
}

export default {
	PostUrl,
	PostFormSystem,
	PostForm,
	FintechPost,
}


