const toDecimal=function(value){
	let a=/([0-9]*[.|,])?[0-9]+/
	let sonuc=a.exec(value);
	let text="";
	if(sonuc)   text= sonuc[0].replace(',','.');
	else        text= "0";
	console.log(text[0])
	if(text[0]==='.') text="0"+text;
	return text;
}
//verdiğiniz değerin verdiğiniz kadar decimal basamaklı olarak geri döndürür.
const toDecimalWithDigit=function(value,digitOfDecimal){//bu fonksiyonda geri dönüşler 1 1. 1.3 fibi yada "" boş değer dönecektir
	let a=new RegExp("[0-9]{1,}[.|,]?[0-9]{0,"+digitOfDecimal+"}");//dinamik reguler expression
	let sonuc=value.match(a);
	let text="";
	if(sonuc) text= (""+sonuc).replace(',','.');
	return text;
}
const toDecimalWritingPositive = function(value){//bu fonksiyonda geri dönüşler 1 1. 1.3 fibi yada "" boş değer dönecektir
	let a=new RegExp("[1-9][0-9]*");//dinamik reguler expression
	let sonuc=value.match(a);
	let text="";
	if(sonuc) text= (""+sonuc).replace(',','.');
	return text;
}

const toFinanceTick=function(value){
	let a=/([0-9]*[.|,][0-9]*[1-9])|([0-9]*[.|,])|([1-9][0-9]*)/
	
	let sonuc=a.exec(value);
	let text="";
	if(sonuc)  text= sonuc[0].replace(',','.');
	
	if(text[0]==='.') text=""+text;
	if(text=="") text="0.01"
	return text;
}
export default {
	toDecimal,
	toDecimalWithDigit,
	toDecimalWritingPositive,
	toFinanceTick
}